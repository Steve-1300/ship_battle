import requestDB from "../db/requestDB.js";

export async function getUsersList(req, res) {
  try {
    const users = await requestDB.getUsers();

    const result = users.result
      .filter((user) => user.roles_id >= 2)
      .map((user) => {
        return { id: user.id, login: user.login };
      });
    res.status(200).json(result);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
}
export async function getUserById(req, res) {
  try {
    const id = req.params.id;
    const user = await requestDB.findUserByID(id);

    res.status(200).json({ id: user.result.id, login: user.result.login });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
}
export async function setUsersOnPool(req, res) {
  try {
    const poolId = req.params.poolId;
    const users = req.body.users;

    await requestDB.deleteUsersOnPool(poolId);
    users.forEach(async (user) => {
      await requestDB.setUsersOnPool(poolId, user);
    });

    res.status(200).json("поле обновлено");
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
}

export async function getUserShots(req, res) {
  try {
    const { pool_id } = req.query;

    const result = await requestDB.getUserShots(req.user.id, pool_id);

    res.status(200).json(result.result.shots);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
}
export async function getUserShotsLog(req, res) {
  try {
    const { pool_id } = req.query;

    const result = await requestDB.getUserShotsLog(req.user.id, pool_id);
    const pool_ships = await requestDB.getShipsFromPool(pool_id);

    result.result.forEach((shot) => {
      pool_ships.result.forEach((ship) => {
        if (shot.x === ship.x && shot.y === ship.y) {
          shot.ship = ship;
        }
      });
    });
    res.status(200).json(result.result);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
}
export async function getUserGifts(req, res) {
  try {
    const result = await requestDB.getUserGifts(req.user.id);
    const gifts = await requestDB.getGifts();
    let userGifts = [];
    result.result.forEach((gift) => {
      userGifts.push(gifts.result.find((item) => gift.gift_id === item.id));
    });
    res.status(200).json(userGifts);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
}
export async function getPoolInfo(req, res) {
  try {
    const poolId = req.query.pool_id;
    const result = await requestDB.getPoolById(poolId);
    res.status(200).json(result.result);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
}
export async function getPoolsFromPoolsUsers(req, res) {
  try {
    const { user_id } = req.query;
    const result = await requestDB.getPoolsFromPoolsUsers(user_id);
    const pools = result.result.map(
      (pool) => (pool.name = requestDB.getPoolById(pool.pool_id))
    );

    Promise.all(pools)
      .then((pools) => {
        pools.forEach((pool, i) => {
          result.result[i].name = pool.result.name;
        });
      })
      .then(() => {
        res.status(200).json(result.result);
      });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
}
export async function getUsersFromPool(req, res) {
  try {
    const { pool_id } = req.params;
    const result = await requestDB.getUsersFromPool(pool_id);

    res.status(200).json(result.result);
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
}
export async function shot(req, res) {
  try {
    const { pool_id, x, y } = req.body;
    const user = req.user;
    const userShots = await requestDB.getUserShotsLog(user.id, pool_id);
    const userInfo = await requestDB.getUserFromPool(user.id, pool_id);
    const ships = await requestDB.getShipsFromPool(pool_id);

    if (userInfo.shots <= userShots.length) {
      res.status(200).json({ message: "Выстрелы закончились" });
      return;
    }
    if (userShots.result.find((shot) => shot.x === x && shot.y === y)) {
      res.status(200).json({ message: "Вы уже стреляли в эту клетку" });
      return;
    }
    requestDB.setShot(user.id, pool_id, x, y);
    const shipOnCell = ships.result.find(
      (ship) => ship.x === x && ship.y === y
    );
    if (!shipOnCell) {
      // Промах
      res.status(200).json({ message: "Вы промахнулись" });
      return;
    }

    const ship = await requestDB.getShipByID(shipOnCell.ship_id);

    requestDB.setUserGift(user.id, ship.result.gift_id);
    const gift = await requestDB.getGiftByID(ship.result.gift_id);
    res.status(200).json({ gift, shipOnCell, message: "Вы попали в корабль" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
}
