import jwt from "jsonwebtoken";
import { hash, compare } from "bcrypt";

import requestDB from "../db/requestDB.js";
import { config } from "dotenv";
config();
// Обработчик для регистрации нового пользователя
export async function register(req, res) {
  try {
    const login = req.body.user.login;
    const password = req.body.user.password;

    // Проверка, что все поля заполнены
    if (!login || !password) {
      return res
        .status(400)
        .json({ message: "Не заполнены обязательные поля" });
    }

    // Проверка, что пользователь с таким логином не существует
    const existingUser = await requestDB.findUserByLogin(login);
    if (existingUser.result) {
      return res
        .status(400)
        .json({ message: `Пользователь с логином ${login} уже существует` });
    }

    // Хеширование пароля
    const hashedPassword = await hash(password, 10);

    // Создание нового пользователя
    const newUser = await requestDB.createUser({
      login,
      password: hashedPassword,
    });

    // Создание JWT токена

    res.status(201).json("регистрация успешна");
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
}
export async function registrationAdmin(req, res) {
  try {
    const { login, password, adminRegisterKey } = req.body.user;

    // Проверка, что все поля заполнены
    if (!login || !password || !adminRegisterKey) {
      return res
        .status(400)
        .json({ message: "Не заполнены обязательные поля" });
    }
    console.log(adminRegisterKey);
    console.log(process.env.ADMIN_KEY);
    if (adminRegisterKey !== process.env.ADMIN_KEY) {
      return res.status(400).json({ message: "Неверный ключ администратора" });
    }

    // Проверка, что пользователь с таким логином не существует
    const existingUser = await requestDB.findUserByLogin(login);
    if (existingUser.result) {
      return res
        .status(400)
        .json({ message: `Пользователь с логином ${login} уже существует` });
    }

    // Хеширование пароля
    const hashedPassword = await hash(password, 10);

    // Создание нового пользователя
    const newUser = await requestDB.createAdmin({
      login,
      password: hashedPassword,
    });

    // Создание JWT токена

    res.status(201).json("регистрация успешна");
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
}

// Обработчик для входа пользователя
export async function login(req, res) {
  try {
    const { login, password } = req.body.user;

    // Проверка, что все поля заполнены
    if (!login || !password) {
      return res.status(400).json({ message: "Заполните все поля" });
    }

    // Проверка, что пользователь с таким login существует
    const user = await requestDB.findUserByLogin(login);
    if (!user.result) {
      return res
        .status(400)
        .json({ message: "Логин или пароль введены неверно" });
    }

    // Проверка пароля
    const isMatch = await compare(password, user.result.password);
    if (!isMatch) {
      return res
        .status(400)
        .json({ message: "Логин или пароль введены неверно" });
    }

    // Создание JWT токена
    const token = jwt.sign({ user_id: user.id }, process.env.JWT_SECRET);
    //записываем токен в базу
    await requestDB.createToken(token, user.result.id);
    delete user.result.password;
    if (user.result.roles_id === 1) {
      user.result.role = "admin";
    } else {
      user.result.role = "user";
    }
    delete user.result.roles_id;
    res.status(200).json({ token, user: user.result });
  } catch (error) {
    console.error(error);

    res.status(500).json({ message: "Server error" });
  }
}

export async function userInfo(req, res) {
  try {
    let token;
    if (req.headers.authorization) {
      token = req.headers.authorization.split(" ")[1];
    }

    if (!token) {
      return res.status(401).json({ message: "Пользователь не авторизован" });
    }

    const user = await requestDB.findUserByToken(token);
    if (!user) {
      return res.status(400).json({ message: "Пользователь не найден" });
    }
    delete user.password;
    if (user.roles_id === 1) {
      user.role = "admin";
    } else {
      user.role = "user";
    }
    delete user.roles_id;
    res.status(200).json({ token, user });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Server error" });
  }
}
