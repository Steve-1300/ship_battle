import requestDB from "../../db/requestDB.js";

export async function createGift(req, res) {
  try {
    const { name, description, img_url } = req.body;
    // Проверка, что все поля заполнены
    if (!name || !description || !img_url) {
      return res.status(400).json({ message: "Заполните все поля" });
    }

    await requestDB.createGift(name, description, img_url);
    return res.status(200).json({ message: "Подарок создан" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
}
export async function editGift(req, res) {
  try {
    const { name, description, img_url } = req.body;
    const id = req.params.id;
    // Проверка, что все поля заполнены
    if (!name || !description || !img_url) {
      return res.status(400).json({ message: "Заполните все поля" });
    }

    await requestDB.editGift(name, description, img_url, id);
    return res.status(200).json({ message: "Подарок изменен" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
}

export async function getGifts(req, res) {
  try {
    const gifts = await requestDB.getGifts();
    res.status(200).json({ gifts: gifts.result });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
}
export async function deleteGift(req, res) {
  try {
    const id = req.params.id;
    const ships = await requestDB.getShips();
    const isUsed = ships.result.some((ship) => ship.gift_id === id);
    if (isUsed) {
      return res.status(400).json({ message: "Подарок используется" });
    }
    await requestDB.deleteGift(id);
    return res.status(200).json({ message: "Подарок удален" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
}
