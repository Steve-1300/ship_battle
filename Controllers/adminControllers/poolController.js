import requestDB from "../../db/requestDB.js";

export async function getPools(req, res) {
  try {
    const pools = await requestDB.getPools();
    res.status(200).json({ pools: pools.result });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
}
export async function createPool(req, res) {
  try {
    const { name, size, description } = req.body;
    // Проверка, что все поля заполнены
    if (!name || !size) {
      return res.status(400).json({ message: "Заполните все поля" });
    }

    await requestDB.createPool(name, size, description);
    return res.status(200).json({ message: "Игровое поле создано" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
}

export async function editPool(req, res) {
  try {
    const { name, size, description } = req.body;
    const pool_id = req.params.id;
    const poolsShots = await requestDB.getPoolLog(pool_id);
    if (poolsShots.result) {
      return res
        .status(400)
        .json({ message: "Вы не можете изменить поле с выстрелами" });
    }
    // Проверка, что все поля заполнены
    if (!name || !size) {
      return res.status(400).json({ message: "Заполните все поля" });
    }

    await requestDB.editPool(name, size, description, id);
    return res.status(200).json({ message: "Игровое поле изменено" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
}

export async function deletePool(req, res) {
  try {
    const pool_id = req.params.id;
    const poolsShots = await requestDB.getPoolLog(pool_id);
    if (poolsShots.result) {
      return res
        .status(400)
        .json({ message: "Вы не можете удалить поле с выстрелами" });
    }
    await requestDB.deletePool(id);
    return res.status(200).json({ message: "Поле удалено" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
}
export async function setShips(req, res) {
  try {
    const poolId = req.params.poolId;
    const ships = req.body; //array [{id: 1, x:0, y: 0,}]
    await requestDB.deleteShipOnPool(poolId);
    ships.forEach(async (ship) => await requestDB.setShipOnPool(poolId, ship));

    return res.status(200).json({ message: `поле ${poolId} обновлено` });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
}
