import requestDB from "../../db/requestDB.js";

export async function createShip(req, res) {
  try {
    const { name, size, gift_id } = req.body;
    // Проверка, что все поля заполнены
    if (!name || !size || !gift_id) {
      return res.status(400).json({ message: "Заполните все поля" });
    }

    await requestDB.createShip(name, size, gift_id);
    return res.status(200).json({ message: "Корабль создан" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
}
export async function editShip(req, res) {
  try {
    const { name, size, gift_id } = req.body;
    const id = req.params.id;
    // Проверка, что все поля заполнены
    if (!name || !size || !gift_id) {
      return res.status(400).json({ message: "Заполните все поля" });
    }

    await requestDB.editShip(name, size, gift_id, id);
    return res.status(200).json({ message: "Корабль изменен" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
}

export async function getShips(req, res) {
  try {
    const ships = await requestDB.getShips();
    res.status(200).json({ ships: ships.result });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
}
export async function deleteShip(req, res) {
  try {
    const id = req.params.id;
    const pools = await requestDB.getPools();
    const isUsed = pools.result.some((pool) => pool.ship_id === id);
    if (isUsed) {
      return res.status(400).json({ message: "Корабль используется" });
    }
    await requestDB.deleteShip(id);
    return res.status(200).json({ message: "Корабль удален" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
}
