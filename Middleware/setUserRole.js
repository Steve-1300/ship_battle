import requestDB from "../db/requestDB.js";

export const setUserRole = async (req, res, next) => {
  if (req.headers.authorization) {
    const token = req.headers.authorization.split(" ")[1];
    if (!token) {
      req.role = "guest";
    } else {
      const user = await requestDB.findUserByToken(token);
      let role = null;

      if (user) {
        req.user = user;
        role = await requestDB.findRoleByID(user.roles_id);
      }
      if (role) {
        req.user.role = role.role;
      } else {
        req.user = { role: { id: 0, role: "guest" } };
      }
    }
  }

  next();
};
