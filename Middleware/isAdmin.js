export const isAdmin = (req, res, next) => {
  if (req.user.role === "admin") {
    next(); // Продолжаем выполнение следующего middleware или маршрута
  } else {
    res.status(403).send("Доступ запрещен"); // Отправляем ошибку доступа запрещен
  }
};
