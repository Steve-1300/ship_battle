import bodyParser from "body-parser";
import express from "express";
import auth from "./Routes/auth.js";
import admin from "./Routes/admin/admin.js";
import userAdmin from "./Routes/admin/user.js";
import user from "./Routes/user.js";
import { setUserRole } from "./Middleware/setUserRole.js";
import cors from "cors";

const app = express();

app.use(bodyParser.json());
app.use(setUserRole);
app.use(cors());

app.use("/auth", auth);
app.use("/user", user);
app.get("/", function (req, res) {
  res.send("Было бы не плохо отдавать по этому запросу страницу");
});

//Запросы с админкой
app.use("/user", userAdmin);
app.use("/", admin);

app.listen(3000);
