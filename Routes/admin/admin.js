import { Router } from "express";
import pool from "./pool.js";
import gift from "./gift.js";
import ship from "./ship.js";
import { isAdmin } from "../../Middleware/isAdmin.js";

const router = Router();

router.use(isAdmin);
router.use("/pool", pool);
router.use("/gift", gift);
router.use("/ship", ship);

export default router;
