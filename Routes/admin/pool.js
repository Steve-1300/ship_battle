import { Router } from "express";
import {
  getPools,
  createPool,
  editPool,
  deletePool,
  setShips,
} from "../../Controllers/adminControllers/poolController.js";
import { getUsersFromPool } from "../../Controllers/userController.js";

const router = Router();

router.get("/list", getPools);
router.post("/create", createPool);
router.post("/edit/:id", editPool);
router.post("/setShips/:poolId", setShips);
router.get("/users/:pool_id", getUsersFromPool);
router.get("/delete/:id", deletePool);

export default router;
