import { Router } from "express";
import {
  createShip,
  getShips,
  editShip,
  deleteShip,
} from "../../Controllers/adminControllers/shipController.js";

const router = Router();

router.get("/list", getShips);
router.post("/create", createShip);
router.post("/edit/:id", editShip);
router.get("/delete/:id", deleteShip);

export default router;
