import { Router } from "express";
import { setUsersOnPool } from "../../Controllers/userController.js";
import { isAdmin } from "../../Middleware/isAdmin.js";

var router = Router();
router.use(isAdmin);
router.post("/setUsersOnPool/:poolId", setUsersOnPool);
export default router;
