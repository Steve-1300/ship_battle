import { Router } from "express";
import {
  createGift,
  getGifts,
  editGift,
  deleteGift,
} from "../../Controllers/adminControllers/giftController.js";

const router = Router();

router.get("/list", getGifts);
router.post("/create", createGift);
router.post("/edit/:id", editGift);
router.get("/delete/:id", deleteGift);

export default router;
