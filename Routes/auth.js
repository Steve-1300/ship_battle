import { Router } from "express";
import {
  register,
  registrationAdmin,
  login,
  userInfo,
} from "../Controllers/authController.js";
const router = Router();

//Registration
router.post("/registration", register);
router.post("/registrationAdmin", registrationAdmin);
router.post("/login", login);
router.get("/userInfo", userInfo);

export default router;
