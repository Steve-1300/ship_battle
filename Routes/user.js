import { Router } from "express";
import {
  getUsersList,
  getUserById,
  getUserShots,
  getUserShotsLog,
  getUserGifts,
  getPoolsFromPoolsUsers,
  getPoolInfo,
  shot,
} from "../Controllers/userController.js";

const router = Router();

router.get("/usersList", getUsersList);
router.get("/byId/:id", getUserById);
router.get("/getUserShots", getUserShots);
router.get("/getUserShotsLog", getUserShotsLog);
router.get("/getPoolInfo", getPoolInfo);
router.get("/getUserGifts", getUserGifts);
router.get("/getPoolsFromPoolsUsers", getPoolsFromPoolsUsers);
router.post("/shot", shot);

export default router;
