import sqlite3 from "sqlite3";
import { createDB, fillDBIfEmpty } from "./createDB.js";

//https://github.com/TryGhost/node-sqlite3/wiki/API#allsql--param---callback
//https://cheatcode.co/tutorials/how-to-use-sqlite-with-node-js

const SQLite3 = sqlite3.verbose();

export const db = new SQLite3.Database("./db/ship_battle.db");

export const query = async (command, method = "all") => {
  return new Promise((resolve, reject) => {
    db[method](command, function (err, result) {
      err ? reject(err) : resolve({ result, id: this.lastID });
    });
  });
};

db.serialize(() => {
  createDB().then(() => fillDBIfEmpty());
});
