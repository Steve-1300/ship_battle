import { query } from "./db.js";
export const createDB = async () => {
  await query(
    "CREATE TABLE IF NOT EXISTS roles (id INTEGER PRIMARY KEY AUTOINCREMENT, role text)",
    "run"
  );
  await query(
    `CREATE TABLE IF NOT EXISTS users 
        (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            login text, password text, 
            roles_id integer, 
            FOREIGN KEY (roles_id) REFERENCES roles (id)
        )`,
    "run"
  );
  await query(
    `CREATE TABLE IF NOT EXISTS user_tokens (
        id INTEGER PRIMARY KEY AUTOINCREMENT, 
        token text,
        user_id INTEGER, 
        FOREIGN KEY (user_id) REFERENCES users (id))`,
    "run"
  );
  await query(
    "CREATE TABLE IF NOT EXISTS pools (id INTEGER PRIMARY KEY AUTOINCREMENT, name text,	description text,	size INTEGER)",
    "run"
  );
  await query(
    "CREATE TABLE IF NOT EXISTS gifts (id INTEGER PRIMARY KEY AUTOINCREMENT, name text,	description	text, url_img text)",
    "run"
  );
  await query(
    "CREATE TABLE IF NOT EXISTS ships (id INTEGER PRIMARY KEY AUTOINCREMENT, name text,	size INTEGER, gift_id INTEGER, FOREIGN KEY (gift_id) REFERENCES gifts (id))",
    "run"
  );
  await query(
    `CREATE TABLE IF NOT EXISTS pools_users (
        shots INTEGER,
        pool_id INTEGER,
        user_id INTEGER,
        FOREIGN KEY (pool_id) REFERENCES pools (id), 
        FOREIGN KEY (user_id) REFERENCES users (id))`,
    "run"
  );
  await query(
    "CREATE TABLE IF NOT EXISTS pools_ships (x INTEGER, y INTEGER, pool_id INTEGER, ship_id INTEGER, FOREIGN KEY (pool_id) REFERENCES pools (id), FOREIGN KEY (ship_id) REFERENCES ships (id))",
    "run"
  );
  await query(
    "CREATE TABLE IF NOT EXISTS pools_shots (x INTEGER, y INTEGER, pool_id INTEGER, user_id INTEGER,FOREIGN KEY (pool_id)  REFERENCES pools (id), FOREIGN KEY (user_id)  REFERENCES users (id))",
    "run"
  );
  await query(
    "CREATE TABLE IF NOT EXISTS users_gifts (id INTEGER PRIMARY KEY AUTOINCREMENT, user_id INTEGER, gift_id INTEGER, FOREIGN KEY (user_id)  REFERENCES users (id), FOREIGN KEY(gift_id) REFERENCES gifts (id))",
    "run"
  );
};

export const fillDBIfEmpty = async () => {
  const users = await query("SELECT * FROM users");
  const role = await query("SELECT * FROM roles");

  if (role.result?.length === 0) {
    await query(`INSERT INTO roles (role) VALUES ('admin')`, "run");
    await query(`INSERT INTO roles (role) VALUES ('user')`, "run");
  }
  if (users.result?.length === 0) {
    await query(
      `INSERT INTO users (login, password, roles_id) VALUES ('admin', '$2b$10$JtKSve92veFyvySz2I18ROorXGjm9cxuus3Z11ViEOqEwVBuel22a', 1)`,
      "run"
    );
  }
};
