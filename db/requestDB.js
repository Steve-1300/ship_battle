import { query } from "./db.js";

async function findUser(user) {
  return new Promise((resolve, reject) => {
    query(
      `SELECT * FROM users WHERE login='${user.login}' AND password='${user.password}'`,
      "get"
    )
      .then((user) => (user ? resolve(user) : reject("Пользователь не найден")))
      .catch((err) => reject(err));
  });
}
async function getUsers(user) {
  return await query(`SELECT * FROM users`);
}

async function findUserByID(id) {
  return new Promise((resolve, reject) => {
    query(`SELECT * FROM users WHERE id=${id}`, "get")
      .then((user) => resolve(user))
      .catch((err) => reject(err));
  });
}

async function findUserByLogin(login) {
  return new Promise((resolve, reject) => {
    query(`SELECT * FROM users WHERE login='${login}'`, "get")
      .then((user) => resolve(user))
      .catch((err) => reject(err));
  });
}

async function findUserByToken(token) {
  return findToken(token).then((userId) => {
    if (!userId.result?.user_id) {
      return null;
    }
    return findUserByID(userId.result?.user_id).then((user) => user.result);
  });
}

async function createUser(user, roleId = 2) {
  return await query(
    `INSERT INTO users (login, password, roles_id) VALUES ('${user.login}', '${user.password}', ${roleId})`,
    "get"
  );
}
async function createAdmin(user) {
  return createUser(user, 1);
}

async function createToken(token, userId) {
  return await query(
    `INSERT INTO user_tokens (token, user_id) VALUES ('${token}', ${userId})`,
    "get"
  );
}

async function findToken(token) {
  return new Promise((resolve, reject) => {
    query(`SELECT * FROM user_tokens WHERE token='${token}'`, "get")
      .then((user) => resolve(user))
      .catch((err) => reject(err));
  });
}
async function findRoleByID(roles_id) {
  return query(`SELECT * FROM roles WHERE id='${roles_id}'`, "get")
    .then((user) => user.result)
    .catch((err) => reject(err));
}

async function deleteToken(token) {
  return await query(`DELETE FROM user_tokens WHERE token='${token}'`, "get");
}

async function createGift(name, description, img_url) {
  return await query(
    `INSERT INTO gifts (name, description, url_img) VALUES ('${name}', '${description}', '${img_url}')`,
    "all"
  );
}
async function createShip(name, size, gift_id) {
  return await query(
    `INSERT INTO ships (name, size, gift_id) VALUES ('${name}', '${size}', '${gift_id}')`,
    "all"
  );
}
async function createPool(name, size, description) {
  return await query(
    `INSERT INTO pools (name, size, description) VALUES ('${name}', '${size}', '${description}')`,
    "all"
  );
}

async function setShipOnPool(pool_id, ship) {
  return await query(
    `INSERT INTO pools_ships (pool_id, ship_id, x, y) VALUES ('${pool_id}', '${ship.id}', '${ship.x}', '${ship.y}')`
  );
}
async function setUsersOnPool(pool_id, user) {
  return await query(
    `INSERT INTO pools_users (pool_id, user_id, shots) VALUES ('${pool_id}', '${user.id}', '${user.shots}')`
  );
}

async function editGift(name, description, img_url, id) {
  return await query(
    `UPDATE gifts
    SET name = '${name}', description='${description}', url_img='${img_url}'
    WHERE id = ${id}`
  );
}
async function editShip(name, size, gift_id, id) {
  return await query(
    `UPDATE ships
    SET name = '${name}', size=${size}, gift_id=${gift_id}
    WHERE id = ${id}`
  );
}
async function editPool(name, size, description, id) {
  return await query(
    `UPDATE pools
    SET name = '${name}', size=${size}, description='${description}'
    WHERE id = ${id}`
  );
}

async function deleteGift(id) {
  return await query(`DELETE FROM gifts WHERE id='${id}'`, "get");
}
async function deleteShip(id) {
  return await query(`DELETE FROM ships WHERE id='${id}'`, "get");
}
async function deletePool(id) {
  return await query(`DELETE FROM pools WHERE id='${id}'`, "get");
}
async function deleteShipOnPool(pool_id) {
  return await query(`DELETE FROM pools_ships WHERE pool_id=${pool_id}`);
}
async function deleteUsersOnPool(pool_id) {
  return await query(`DELETE FROM pools_users WHERE pool_id=${pool_id}`);
}

async function getGifts() {
  return await query(`SELECT * FROM gifts`);
}
async function getShips() {
  return await query(`SELECT * FROM ships`);
}
async function getPools() {
  return await query(`SELECT * FROM pools`);
}
async function getPoolById(id) {
  return await query(`SELECT * FROM pools where id=${id}`, "get");
}
async function getUserShots(user_id, pool_id) {
  return await query(
    `SELECT * FROM pools_users where user_id=${user_id} and pool_id=${pool_id}`,
    "get"
  );
}
async function getUserShotsLog(user_id, pool_id) {
  return await query(
    `SELECT * FROM pools_shots where user_id=${user_id} and pool_id=${pool_id}`
  );
}
async function getPoolLog(pool_id) {
  return await query(`SELECT * FROM pools_shots where pool_id=${pool_id}`);
}
async function getUserGifts(user_id) {
  return await query(`SELECT * FROM users_gifts where user_id=${user_id}`);
}

async function getUsersFromPool(pool_id) {
  return await query(`SELECT * FROM pools_users where  pool_id=${pool_id}`);
}
async function getPoolsFromPoolsUsers(user_id) {
  return await query(`SELECT * FROM pools_users where  user_id=${user_id}`);
}
async function getUserFromPool(user_id, pool_id) {
  return await query(
    `SELECT * FROM pools_users where user_id=${user_id} and pool_id=${pool_id}`
  );
}
async function getShipsFromPool(pool_id) {
  return await query(`SELECT * FROM pools_ships where  pool_id=${pool_id}`);
}
async function getGiftByID(gift_id) {
  return await query(`SELECT * FROM gifts where id=${gift_id}`, "get");
}
async function getShipByID(ship_id) {
  return await query(`SELECT * FROM ships where id=${ship_id}`, "get");
}
async function setShot(user_id, pool_id, x, y) {
  return await query(
    `INSERT INTO pools_shots (user_id, pool_id, x, y) VALUES ('${user_id}', '${pool_id}', '${x}', '${y}')`
  );
}
async function setUserGift(user_id, gift_id) {
  return await query(
    `INSERT INTO  users_gifts (user_id, gift_id) VALUES ('${user_id}', '${gift_id}')`
  );
}

const requestDB = {
  findUser,
  getUsers,
  findUserByLogin,
  findUserByID,
  createUser,
  createAdmin,
  createToken,
  findToken,
  deleteToken,
  findUserByToken,
  createGift,
  getGifts,
  findRoleByID,
  editGift,
  deleteGift,
  getShips,
  deleteShip,
  editShip,
  createShip,
  editPool,
  getPools,
  createPool,
  deletePool,
  deleteShipOnPool,
  setShipOnPool,
  deleteUsersOnPool,
  setUsersOnPool,
  getUserShots,
  getUserGifts,
  getPoolsFromPoolsUsers,
  getUserFromPool,
  getShipsFromPool,
  setShot,
  setUserGift,
  getGiftByID,
  getPoolById,
  getUserShotsLog,
  getShipByID,
  getUsersFromPool,
  getPoolLog,
};

export default requestDB;
