import { defineStore } from "pinia";
import { ref, computed, watch, onMounted } from "vue";
import { useApi } from "../api/index.js";
import { useRouter } from "vue-router";

export const useUserStore = defineStore("UserStore", () => {
  const router = useRouter();

  const api = useApi();
  const token = ref("");
  const name = ref("");
  const id = ref(null);
  const isAdmin = ref(false);
  const gifts = ref([]);

  function registration(login, password, adminRegisterKey) {
    if (adminRegisterKey) {
      api.registrationAdmin({ login, password, adminRegisterKey });
    } else {
      api.registration({ login, password });
    }
  }
  function login(login, password) {
    api
      .login({ login, password })
      .then((res) => {
        setUser(res);
        api.setToken(res.token);

        isAdmin.value
          ? router.push({ name: "admin-pools" })
          : router.push({ name: "users-pools" });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  function logout() {
    token.value = "";
    api.deleteToken();
    name.value = "";
    id.value = "";
    router.push({ path: "/" });
  }

  function setUser(data) {
    name.value = data.user.login;
    token.value = data.token;
    id.value = data.user.id;
    isAdmin.value = data.user.role === "admin";
  }

  async function getUserInfo() {
    token.value = api.getToken();
    if (token.value) {
      await api.getUser().then((res) => setUser(res));
    }
  }

  async function getUserShots(pool_id) {
    return await api.getUserShots(pool_id);
  }
  async function getUserShotsLog(pool_id) {
    return await api.getUserShotsLog(pool_id);
  }
  async function requestGifts() {
    gifts.value = await api.getUserGifts(id.value);
  }

  async function getUsersPools() {
    return await api.getPoolsFromPoolsUsers(id.value);
  }
  async function getPoolInfo(pool_id) {
    return await api.getPoolInfo(pool_id);
  }
  async function shot(pool_id, x, y) {
    return await api.shot(pool_id, x, y);
  }

  onMounted(async () => {
    if (!id.value) {
      getUserInfo();
    }
  });

  return {
    token,
    name,
    id,
    login,
    logout,
    registration,
    gifts,
    isAdmin,
    requestGifts,
    getUserShots,
    getUsersPools,
    getUserInfo,
    getPoolInfo,
    getUserShotsLog,
    shot,
  };
});
