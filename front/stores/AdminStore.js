import { defineStore } from "pinia";
import { ref, computed, watch, onMounted } from "vue";
import { useApi } from "../api/index.js";

export const useAdminStore = defineStore("useAdminStore", () => {
  const api = useApi();
  const gifts = ref([]);
  const ships = ref([]);
  const pools = ref([]);
  const users = ref([]);

  async function getGifts() {
    api.getGifts().then((data) => {
      gifts.value = data.gifts;
    });
  }
  async function createGift(gift) {
    await api.createGift(gift);
    getGifts();
  }
  async function editGift(gift, id) {
    await api.editGift(gift, id);
    getGifts();
  }
  async function deleteGift(id) {
    await api.deleteGift(id);
    getGifts();
  }
  async function getShips() {
    return api.getShips().then((data) => {
      ships.value = data.ships;
    });
  }
  async function createShip(gift) {
    await api.createShip(gift);
    getShips();
  }
  async function editShip(gift, id) {
    await api.editShip(gift, id);
    getShips();
  }
  async function deleteShip(id) {
    await api.deleteShip(id);
    getShips();
  }

  async function getPools() {
    api.getPools().then((data) => {
      pools.value = data.pools;
    });
  }
  async function createPool(gift) {
    console.log(123123);
    await api.createPool(gift);
    getPools();
  }
  async function editPool(gift, id) {
    await api.editPool(gift, id);
    getPools();
  }
  async function deletePool(id) {
    await api.deletePool(id);
    getPools();
  }
  async function setShipsOnPool(poolId, ships) {
    await api.setShipsOnPool(poolId, ships);
  }
  async function getUserById(userId) {
    return await api.getUserById(userId);
  }
  async function requestUsers() {
    users.value = await api.getUsers();
  }
  async function setUsersOnPool(poolId, users) {
    return await api.setUsersOnPool(poolId, users);
  }

  async function getUsersOnPool(pool_id) {
    return await api.getUsersOnPool(pool_id);
  }

  return {
    gifts,
    ships,
    pools,
    users,
    getShips,
    getGifts,
    createGift,
    editGift,
    deleteGift,
    createShip,
    editShip,
    deleteShip,
    getPools,
    createPool,
    editPool,
    deletePool,
    setShipsOnPool,
    getUserById,
    requestUsers,
    setUsersOnPool,
    getUsersOnPool,
  };
});
