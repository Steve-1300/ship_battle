import { createApp } from "vue";
import { createPinia } from "pinia";
import "./style.css";
import App from "./App.vue";
import router from "./router/router.js";
import CKEditor from "@ckeditor/ckeditor5-vue";

const app = createApp(App);
const pinia = createPinia();

app.use(pinia).use(router).use(CKEditor).mount("#app");
