import AppLayoutDefault from "../components/AppLayoutDefault.vue";
import PageNotFound from "../pages/PageNotFound.vue";
import TheAuth from "../pages/TheAuth.vue";
import TheMain from "../pages/TheMain.vue";
import UsersProfile from "../pages/UsersProfile.vue";
import UsersPools from "../pages/UsersPools.vue";
import AdminPools from "../pages/AdminPools.vue";
import AdminShips from "../pages/AdminShips.vue";
import AdminGifts from "../pages/AdminGifts.vue";
import TheGifts from "../pages/TheGifts.vue";
import TheGame from "../pages/TheGame.vue";

export const routes = [
  {
    path: "/",
    name: "base",
    component: AppLayoutDefault,
    children: [
      {
        path: "/",
        name: "main",
        component: TheMain,
      },
      {
        path: "/auth",
        name: "auth",
        component: TheAuth,
      },
      {
        path: "/register-admin",
        name: "register-admin",
        meta: { isAdmin: true },
        component: TheAuth,
      },
      {
        path: "/user/pools",
        name: "users-pools",
        component: UsersPools,
      },

      {
        path: "/user/profile",
        name: "users-profile",
        component: UsersProfile,
      },
      {
        path: "/admin/pools",
        name: "admin-pools",
        component: AdminPools,
      },
      {
        path: "/admin/ships",
        name: "admin-ships",
        component: AdminShips,
      },
      {
        path: "/admin/gifts",
        name: "admin-gifts",
        component: AdminGifts,
      },

      {
        path: "/user/gifts",
        name: "user-gifts",
        component: TheGifts,
      },
      {
        path: "/game/:id",
        name: "game",
        component: TheGame,
      },
    ],
  },
  {
    path: "/:pathMatch(.*)*",
    name: "not-found",
    component: PageNotFound,
  },
];
