import localStorageService from "./localStorage.js";
import { useAuthApi } from "./auth.js";
import { useAdminApi } from "./admin.js";
import { useUsersApi } from "./user.js";
export function useApi() {
  const authApi = useAuthApi();
  const adminApi = useAdminApi();
  const usersApi = useUsersApi();
  return {
    ...localStorageService,
    ...authApi,
    ...adminApi,
    ...usersApi,
  };
}
