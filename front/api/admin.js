import { useHTTPClient } from "./HTTPClient.js";

export function useAdminApi() {
  const HTTPClient = useHTTPClient();
  //gists
  async function getGifts() {
    return await HTTPClient.get("http://localhost:3000/gift/list");
  }
  async function createGift(gift) {
    return await HTTPClient.post("http://localhost:3000/gift/create", gift);
  }

  async function editGift(gift, id) {
    return await HTTPClient.post(`http://localhost:3000/gift/edit/${id}`, gift);
  }
  async function deleteGift(id) {
    return await HTTPClient.get(`http://localhost:3000/gift/delete/${id}`);
  }
  //ships
  async function getShips() {
    return await HTTPClient.get("http://localhost:3000/ship/list");
  }
  async function createShip(ship) {
    return await HTTPClient.post("http://localhost:3000/ship/create", ship);
  }

  async function editShip(ship, id) {
    return await HTTPClient.post(`http://localhost:3000/ship/edit/${id}`, ship);
  }
  async function deleteShip(id) {
    return await HTTPClient.get(`http://localhost:3000/ship/delete/${id}`);
  }
  //pools
  async function getPools() {
    return await HTTPClient.get("http://localhost:3000/pool/list");
  }
  async function createPool(pool) {
    return await HTTPClient.post("http://localhost:3000/pool/create", pool);
  }

  async function editPool(pool, id) {
    return await HTTPClient.post(`http://localhost:3000/pool/edit/${id}`, pool);
  }
  async function getUsersOnPool(pool_id) {
    return await HTTPClient.get(`http://localhost:3000/pool/users/${pool_id}`);
  }

  async function deletePool(id) {
    return await HTTPClient.get(`http://localhost:3000/pool/delete/${id}`);
  }
  async function setShipsOnPool(poolId, ships) {
    return await HTTPClient.post(
      `http://localhost:3000/pool/setShips/${poolId}`,
      ships
    );
  }

  return {
    createGift,
    getGifts,
    editGift,
    deleteGift,
    getShips,
    createShip,
    editShip,
    deleteShip,
    getPools,
    createPool,
    editPool,
    deletePool,
    setShipsOnPool,
    getUsersOnPool,
  };
}
