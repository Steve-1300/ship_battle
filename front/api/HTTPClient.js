import { useUserStore } from "../stores/UserStore";
import { watch } from "vue";

export function useHTTPClient() {
  const userStore = useUserStore();
  const headers = {};

  headers.Authorization = "Bearer " + localStorage.getItem("token");

  watch(
    () => userStore.token,
    (token) => {
      if (token) {
        headers.Authorization = "Bearer " + token;
      }
    }
  );

  async function get(url, params) {
    return await fetch(url, {
      method: "GET",
      headers,
    })
      .then((res) => {
        if (!res.ok) {
          throw res;
        } else {
          return res.json();
        }
      })
      .then((data) => data)
      .catch((err) => {
        console.log(err);
        throw err;
      });
  }
  async function post(url, params) {
    return await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        ...headers,
      },
      body: JSON.stringify(params),
    })
      .then((res) => {
        if (!res.ok) {
          throw res;
        } else {
          return res.json();
        }
      })
      .then((data) => data)
      .catch((err) => {
        console.log(err);
        throw err;
      });
  }

  return { get, post };
}
