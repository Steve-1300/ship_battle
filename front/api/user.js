import { useUserStore } from "../stores/UserStore";
import { useHTTPClient } from "./HTTPClient.js";

export function useUsersApi() {
  const userStore = useUserStore();
  const HTTPClient = useHTTPClient();

  async function getUserById(id) {
    return await HTTPClient.get("http://localhost:3000/user/byId/" + id);
  }
  async function getUsers() {
    return await HTTPClient.get("http://localhost:3000/user/usersList");
  }
  async function getUserShots(pool_id, user_id) {
    return await HTTPClient.get(
      `http://localhost:3000/user/getUserShots?pool_id=${pool_id}`
    );
  }
  async function getUserShotsLog(pool_id, user_id) {
    return await HTTPClient.get(
      `http://localhost:3000/user/getUserShotsLog?pool_id=${pool_id}`
    );
  }
  async function getUserGifts() {
    return await HTTPClient.get(`http://localhost:3000/user/getUserGifts`);
  }
  async function getPoolInfo(pool_id) {
    return await HTTPClient.get(
      `http://localhost:3000/user/getPoolInfo?pool_id=${pool_id}`
    );
  }
  async function getPoolsFromPoolsUsers(user_id) {
    return await HTTPClient.get(
      `http://localhost:3000/user/getPoolsFromPoolsUsers?user_id=${user_id}`
    );
  }
  async function shot(pool_id, x, y) {
    return await HTTPClient.post("http://localhost:3000/user/shot", {
      pool_id,
      x,
      y,
    });
  }
  async function setUsersOnPool(poolId, users) {
    return await HTTPClient.post(
      "http://localhost:3000/user/setUsersOnPool/" + poolId,
      { users }
    );
  }
  return {
    getUserById,
    getUsers,
    setUsersOnPool,
    getUserShots,
    getUserGifts,
    getPoolsFromPoolsUsers,
    getPoolInfo,
    getUserShotsLog,
    shot,
  };
}
