import { useUserStore } from "../stores/UserStore";
import { useHTTPClient } from "./HTTPClient.js";

export function useAuthApi() {
  const userStore = useUserStore();
  const HTTPClient = useHTTPClient();

  async function login(user) {
    return await HTTPClient.post("http://localhost:3000/auth/login", { user });
  }

  async function registration(user) {
    try {
      const result = await HTTPClient.post(
        "http://localhost:3000/auth/registration",
        {
          user,
        }
      );
      alert("регистрация успешна");
    } catch (error) {
      alert("Ошибка регистрации");
    }
  }
  async function registrationAdmin(user) {
    try {
      const result = await HTTPClient.post(
        "http://localhost:3000/auth/registrationAdmin",
        {
          user,
        }
      );
      alert("регистрация успешна");
    } catch (error) {
      alert("Ошибка регистрации");
    }
  }
  async function getUser() {
    return await HTTPClient.get("http://localhost:3000/auth/userInfo");
  }

  return {
    login,
    registration,
    registrationAdmin,
    getUser,
  };
}
